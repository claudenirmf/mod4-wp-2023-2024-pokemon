package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import nl.utwente.mod4.pokemon.model.Pokemon;

import java.util.ArrayList;
import java.util.List;

@Path("/pokemon")
public class PokemonRoute {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pokemon> getPokemon() {
        var list = new ArrayList<Pokemon> ();
        var bolt = new Pokemon();

        bolt.id = "1";
        bolt.name = "Bolt";
        bolt.trainerId = "1";
        bolt.pokemonType = "513";

        list.add(bolt);
        return list;
    }

    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Pokemon getPokemon(@PathParam("id") String id) {
        var bolt = new Pokemon();

        bolt.id = id;
        bolt.name = "Bolt";
        bolt.trainerId = "1";
        bolt.pokemonType = "513";

        return bolt;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pokemon createPokemon(Pokemon pokemon) {
        return pokemon;
    }

    @Path("/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pokemon updatePokemon(@PathParam("id") String id, Pokemon pokemon) {
        return pokemon;
    }

    @Path("/{id}")
    @DELETE
    public void deletePokemon(@PathParam("id") String id) {
        System.out.println("Pokemon '"+id+"' deleted");
    }
}
